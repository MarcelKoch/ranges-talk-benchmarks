#include <ranges>
#include <vector>
#include <list>
#include <algorithm>
#include <numeric>
#include <cmath>
#include "benchmark/benchmark.h"

template<typename R, typename T>
T accumulate(R&& r, T init){
  return std::accumulate(begin(r), end(r), init);
}

template <typename T = double>
auto init(std::size_t n){
    std::vector<T> v(n);
    std::srand(0);
    std::ranges::generate(v, [](){ return std::rand() - (RAND_MAX / 2);});
    return v;
}


void no_ranges_filtered_sum(benchmark::State& state){
    auto v = init(1000);

    for(auto _: state){
        double sum = 0.;
        for(auto d: v)
            if (d > 0) benchmark::DoNotOptimize(sum += (d * d));
    }
}
BENCHMARK(no_ranges_filtered_sum);


void with_ranges_filtered_sum(benchmark::State& state){
    auto v = init(1000);

    for(auto _: state){
        double sum = 0;
        for(auto d: v | std::views::filter([](const auto t){ return t > 0;})
                    | std::views::transform([](const auto t){ return t * t;}))
            benchmark::DoNotOptimize(sum += d);
    }
}

BENCHMARK(with_ranges_filtered_sum);

void no_ranges_filtered_sum_int(benchmark::State& state){
    auto v = init<int>(1000);

    for(auto _: state){
        int sum = 0;
        for(auto d: v)
            if (d > 0) benchmark::DoNotOptimize(sum += (d * d));
    }
}
BENCHMARK(no_ranges_filtered_sum_int);


void with_ranges_filtered_sum_int(benchmark::State& state){
    auto v = init<int>(1000);

    for(auto _: state){
        int sum = 0;
        for(auto d: v | std::views::filter([](const auto t){ return t > 0;})
                    | std::views::transform([](const auto t){ return t * t;}))
            benchmark::DoNotOptimize(sum += d);
    }
}
BENCHMARK(with_ranges_filtered_sum_int);


void no_ranges_two_norm(benchmark::State& state){
    auto v = init(1000);

  for(auto _: state){
    double two_norm = 0.;
    for(auto d: v)
      two_norm += d * d;
    benchmark::DoNotOptimize(std::sqrt(two_norm));
  }
}
BENCHMARK(no_ranges_two_norm);


void with_ranges_two_norm(benchmark::State& state){
    auto v = init(1000);

  for(auto _: state){
    benchmark::DoNotOptimize(std::sqrt(accumulate(v | std::views::transform([](const auto d)
                                                                            {return d * d;}), 0.)));
  }
}
BENCHMARK(with_ranges_two_norm);


void no_ranges_tail(benchmark::State& state){
    auto v = init(1000);

    for(auto _: state){
        double two_norm = 0.;
        for (int i = v.size() - 100 - 1; i < v.size(); ++i) {
            two_norm += v[i];
        }
        benchmark::DoNotOptimize(two_norm);
    }
}
BENCHMARK(no_ranges_tail);


void with_ranges_tail(benchmark::State& state){
    auto v = init(1000);

    for(auto _: state){
        benchmark::DoNotOptimize(accumulate(v | std::views::reverse | std::views::take(100)
                                            | std::views::reverse, 0.));
    }
}
BENCHMARK(with_ranges_tail);


void with_ranges_tail_reverse(benchmark::State& state){
    auto v = init(1000);

    for(auto _: state){
        benchmark::DoNotOptimize(accumulate(v | std::views::reverse | std::views::take(100), 0.));
    }
}
BENCHMARK(with_ranges_tail_reverse);


void no_ranges_list_tail(benchmark::State& state){
    auto v = init(1000);
    std::list<double> l(begin(v), end(v));

    for(auto _: state){
        double sum = 0.;
        auto it = l.end();
        for (int i = 0; i < 100; ++i) {
            it--;
        }
        for (; it != l.end(); ++it) {
            sum += *it;
        }
        benchmark::DoNotOptimize(sum);
    }
}
BENCHMARK(no_ranges_list_tail);


void with_ranges_list_tail(benchmark::State& state){
    auto v = init(1000);
    std::list<double> l(begin(v), end(v));

    for(auto _: state){
        benchmark::DoNotOptimize(accumulate(l | std::views::reverse | std::views::take(100)
                                              | std::views::reverse, 0.));
    }
}
BENCHMARK(with_ranges_list_tail);


BENCHMARK_MAIN();
